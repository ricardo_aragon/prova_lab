---
home: true
actionText: Documentation
actionLink: /html
features:
- title: Developers
  details: Mirko Agarla and Ricardo Matamoros
- title: School
  details: University of Milano Bicocca - Master degree in informatics
- title: Exam
  details: Design Lab
footer: Copyright © 2019–2020 Mirko Agarla | Ricardo Matamoros
---