module.exports = {
 title: "What do people think?",
 description: "Il progetto si basa sullo sviluppo di un sistema in grado di restituire una distribuzione di probabilità sentiment/aspect di documenti recuperati da diverse fonti, per poter effettuare analisi legate a qualità e soddisfazione sugli articoli analizzati",
 dest: "public",
 base: "/prova_lab/",
 themeConfig: {
   sidebar: ['/' , '/html', '/apiserver' ]
 }
}